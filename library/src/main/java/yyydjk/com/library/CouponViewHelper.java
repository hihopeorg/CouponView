package yyydjk.com.library;

import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.app.Context;

public class CouponViewHelper {

    private static final int DEFAULT_SEMICIRCLE_GAP = 4;
    private static final int DEFAULT_DASH_LINE_LENGTH = 10;
    private static final int DEFAULT_SEMICIRCLE_RADIUS = 4;
    private static final int DEFAULT_SEMICIRCLE_COLOR = 0xFFFFFFFF;
    private static final int DEFAULT_DASH_LINE_HEIGHT = 1;
    private static final int DEFAULT_DASH_LINE_GAP = 5;
    private static final int DEFAULT_DASH_LINE_COLOR = 0xFFFFFFFF;
    private static final int DEFAULT_DASH_LINE_MARGIN = 10;

    private Context context;

    private Component component;

    //半圆画笔
    private Paint semicirclePaint;

    //虚线画笔
    private Paint dashLinePaint;

    //半圆之间间距
    private float semicircleGap;

    //半圆半径
    private float semicircleRadius;

    //半圆颜色
    private Color semicircleColor = new Color(DEFAULT_SEMICIRCLE_COLOR);

    //半圆数量X
    private int semicircleNumX;

    //半圆数量Y
    private int semicircleNumY;

    //绘制半圆曲线后X轴剩余距离
    private int remindSemicircleX;

    //绘制半圆曲线后Y轴剩余距离
    private int remindSemicircleY;

    //虚线的长度
    private float dashLineLength;

    //虚线的高度
    private float dashLineHeight;

    //虚线的间距
    private float dashLineGap;

    //虚线的颜色
    private Color dashLineColor = new Color(DEFAULT_DASH_LINE_COLOR);

    //绘制虚线后X轴剩余距离
    private int remindDashLineX;

    //绘制虚线后Y轴剩余距离
    private int remindDashLineY;

    //虚线数量X
    private int dashLineNumX;

    //半圆数量Y
    private int dashLineNumY;

    //开启顶部半圆曲线
    private boolean isSemicircleTop = true;

    //开启底部半圆曲线
    private boolean isSemicircleBottom = true;

    //开启左边半圆曲线
    private boolean isSemicircleLeft = false;

    //开启右边半圆曲线
    private boolean isSemicircleRight = false;

    //开启顶部虚线
    private boolean isDashLineTop = false;

    //开启底部虚线
    private boolean isDashLineBottom = false;

    //开启左边虚线
    private boolean isDashLineLeft = true;

    //开启左边虚线
    private boolean isDashLineRight = true;

    //view宽度
    private int viewWidth;

    //view的高度
    private int viewHeight;

    //顶部虚线距离View顶部的距离
    private float dashLineMarginTop;

    //底部虚线距离View底部的距离
    private float dashLineMarginBottom;

    //左侧虚线距离View左侧的距离
    private float dashLineMarginLeft;

    //右侧虚线距离View右侧的距离
    private float dashLineMarginRight;


    public CouponViewHelper(Component component, Context context, AttrSet attrSet, String defStyle) {
        this.context = context;
        this.component = component;
        semicircleGap = vp2Px(DEFAULT_SEMICIRCLE_GAP);
        semicircleRadius = vp2Px(DEFAULT_SEMICIRCLE_RADIUS);
        dashLineLength = vp2Px(DEFAULT_DASH_LINE_LENGTH);
        dashLineHeight = vp2Px(DEFAULT_DASH_LINE_HEIGHT);
        dashLineGap = vp2Px(DEFAULT_DASH_LINE_GAP);
        dashLineMarginTop = vp2Px(DEFAULT_DASH_LINE_MARGIN);
        dashLineMarginBottom = vp2Px(DEFAULT_DASH_LINE_MARGIN);
        dashLineMarginLeft = vp2Px(DEFAULT_DASH_LINE_MARGIN);
        dashLineMarginRight = vp2Px(DEFAULT_DASH_LINE_MARGIN);
        if (attrSet != null) {
            if (attrSet.getAttr("cv_semicircle_radius").isPresent()) {
                semicircleRadius = attrSet.getAttr("cv_semicircle_radius").get().getDimensionValue();
            }
            if (attrSet.getAttr("cv_semicircle_gap").isPresent()) {
                semicircleGap = attrSet.getAttr("cv_semicircle_gap").get().getDimensionValue();
            }
            if (attrSet.getAttr("cv_semicircle_color").isPresent()) {
                semicircleColor = attrSet.getAttr("cv_semicircle_color").get().getColorValue();
            }

            if (attrSet.getAttr("cv_dash_line_gap").isPresent()) {
                dashLineGap = attrSet.getAttr("cv_dash_line_gap").get().getDimensionValue();
            }
            if (attrSet.getAttr("cv_dash_line_height").isPresent()) {
                dashLineHeight = attrSet.getAttr("cv_dash_line_height").get().getDimensionValue();
            }
            if (attrSet.getAttr("cv_dash_line_length").isPresent()) {
                dashLineLength = attrSet.getAttr("cv_dash_line_length").get().getDimensionValue();
            }
            if (attrSet.getAttr("cv_dash_line_color").isPresent()) {
                dashLineColor = attrSet.getAttr("cv_dash_line_color").get().getColorValue();
            }

            if (attrSet.getAttr("cv_is_semicircle_top").isPresent()) {
                isSemicircleTop = attrSet.getAttr("cv_is_semicircle_top").get().getBoolValue();
            }
            if (attrSet.getAttr("cv_is_semicircle_bottom").isPresent()) {
                isSemicircleBottom = attrSet.getAttr("cv_is_semicircle_bottom").get().getBoolValue();
            }
            if (attrSet.getAttr("cv_is_semicircle_left").isPresent()) {
                isSemicircleLeft = attrSet.getAttr("cv_is_semicircle_left").get().getBoolValue();
            }
            if (attrSet.getAttr("cv_is_semicircle_right").isPresent()) {
                isSemicircleRight = attrSet.getAttr("cv_is_semicircle_right").get().getBoolValue();
            }
            if (attrSet.getAttr("cv_is_dash_line_top").isPresent()) {
                isDashLineTop = attrSet.getAttr("cv_is_dash_line_top").get().getBoolValue();
            }
            if (attrSet.getAttr("cv_is_dash_line_bottom").isPresent()) {
                isDashLineBottom = attrSet.getAttr("cv_is_dash_line_bottom").get().getBoolValue();
            }
            if (attrSet.getAttr("cv_is_dash_line_left").isPresent()) {
                isDashLineLeft = attrSet.getAttr("cv_is_dash_line_left").get().getBoolValue();
            }
            if (attrSet.getAttr("cv_is_dash_line_right").isPresent()) {
                isDashLineRight = attrSet.getAttr("cv_is_dash_line_right").get().getBoolValue();
            }

            if (attrSet.getAttr("cv_dash_line_margin_top").isPresent()) {
                dashLineMarginTop = attrSet.getAttr("cv_dash_line_margin_top").get().getDimensionValue();
            }
            if (attrSet.getAttr("cv_dash_line_margin_bottom").isPresent()) {
                dashLineMarginBottom = attrSet.getAttr("cv_dash_line_margin_bottom").get().getDimensionValue();
            }
            if (attrSet.getAttr("cv_dash_line_margin_left").isPresent()) {
                dashLineMarginLeft = attrSet.getAttr("cv_dash_line_margin_left").get().getDimensionValue();
            }
            if (attrSet.getAttr("cv_dash_line_margin_right").isPresent()) {
                dashLineMarginRight = attrSet.getAttr("cv_dash_line_margin_right").get().getDimensionValue();
            }
        }
        init();
    }

    private void init() {
        semicirclePaint = new Paint();
        semicirclePaint.setDither(true);
        semicirclePaint.setColor(semicircleColor);
        semicirclePaint.setStyle(Paint.Style.FILL_STYLE);

        dashLinePaint = new Paint();
        dashLinePaint.setDither(true);
        dashLinePaint.setColor(dashLineColor);
        dashLinePaint.setStyle(Paint.Style.FILL_STYLE);
    }

    public void onSizeChanged(int w, int h) {
        viewWidth = w;
        viewHeight = h;
        calculate();
    }

    private void calculate() {
        if (isSemicircleTop || isSemicircleBottom) {
            remindSemicircleX = (int) ((viewWidth - semicircleGap) % (2 * semicircleRadius + semicircleGap));
            semicircleNumX = (int) ((viewWidth - semicircleGap) / (2 * semicircleRadius + semicircleGap));
        }

        if (isSemicircleLeft || isSemicircleRight) {
            remindSemicircleY = (int) ((viewHeight - semicircleGap) % (2 * semicircleRadius + semicircleGap));
            semicircleNumY = (int) ((viewHeight - semicircleGap) / (2 * semicircleRadius + semicircleGap));
        }

        if (isDashLineTop || isDashLineBottom) {
            remindDashLineX = (int) ((viewWidth + dashLineGap - dashLineMarginLeft - dashLineMarginRight) % (dashLineLength + dashLineGap));
            dashLineNumX = (int) ((viewWidth + dashLineGap - dashLineMarginLeft - dashLineMarginRight) / (dashLineLength + dashLineGap));
        }

        if (isDashLineLeft || isDashLineRight) {
            remindDashLineY = (int) ((viewHeight + dashLineGap - dashLineMarginTop - dashLineMarginBottom) % (dashLineLength + dashLineGap));
            dashLineNumY = (int) ((viewHeight + dashLineGap - dashLineMarginTop - dashLineMarginBottom) / (dashLineLength + dashLineGap));
        }
    }

    public void onDraw(Canvas canvas) {
        canvas.save();

        if (isSemicircleTop)
            for (int i = 0; i < semicircleNumX; i++) {
                float x = semicircleGap + semicircleRadius + (remindSemicircleX >> 1) + (semicircleGap + semicircleRadius * 2) * i;
                canvas.drawCircle(x, 0, semicircleRadius, semicirclePaint);
            }
        if (isSemicircleBottom)
            for (int i = 0; i < semicircleNumX; i++) {
                float x = semicircleGap + semicircleRadius + (remindSemicircleX >> 1) + (semicircleGap + semicircleRadius * 2) * i;
                canvas.drawCircle(x, viewHeight, semicircleRadius, semicirclePaint);
            }
        if (isSemicircleLeft)
            for (int i = 0; i < semicircleNumY; i++) {
                float y = semicircleGap + semicircleRadius + (remindSemicircleY >> 1) + (semicircleGap + semicircleRadius * 2) * i;
                canvas.drawCircle(0, y, semicircleRadius, semicirclePaint);
            }
        if (isSemicircleRight)
            for (int i = 0; i < semicircleNumY; i++) {
                float y = semicircleGap + semicircleRadius + (remindSemicircleY >> 1) + (semicircleGap + semicircleRadius * 2) * i;
                canvas.drawCircle(viewWidth, y, semicircleRadius, semicirclePaint);
            }
        if (isDashLineTop)
            for (int i = 0; i < dashLineNumX; i++) {
                float x = dashLineMarginLeft + (remindDashLineX >> 1) + (dashLineGap + dashLineLength) * i;
                canvas.drawRect(x, dashLineMarginTop, x + dashLineLength, dashLineMarginTop + dashLineHeight, dashLinePaint);
            }
        if (isDashLineBottom)
            for (int i = 0; i < dashLineNumX; i++) {
                float x = dashLineMarginLeft + (remindDashLineX >> 1) + (dashLineGap + dashLineLength) * i;
                canvas.drawRect(x, viewHeight - dashLineHeight - dashLineMarginBottom, x + dashLineLength, viewHeight - dashLineMarginBottom, dashLinePaint);
            }
        if (isDashLineLeft)
            for (int i = 0; i < dashLineNumY; i++) {
                float y = dashLineMarginTop + (remindDashLineY >> 1) + (dashLineGap + dashLineLength) * i;
                canvas.drawRect(dashLineMarginLeft, y, dashLineMarginLeft + dashLineHeight, y + dashLineLength, dashLinePaint);
            }
        if (isDashLineRight)
            for (int i = 0; i < dashLineNumY; i++) {
                float y = dashLineMarginTop + (remindDashLineY >> 1) + (dashLineGap + dashLineLength) * i;
                canvas.drawRect(viewWidth - dashLineMarginRight - dashLineHeight, y, viewWidth - dashLineMarginRight, y + dashLineLength, dashLinePaint);
            }

        canvas.restore();
    }

    private int vp2Px(float vp) {
        return (int) (vp * AttrHelper.getDensity(context));
    }

    private int px2Vp(float px) {
        return (int) (px / AttrHelper.getDensity(context));
    }

    public float getSemicircleGap() {
        return px2Vp(semicircleGap);
    }

    public void setSemicircleGap(float semicircleGap) {
        if (this.semicircleGap != semicircleGap) {
            this.semicircleGap = semicircleGap;
            calculate();
            component.invalidate();
        }
    }

    public float getSemicircleRadius() {
        return px2Vp(semicircleRadius);
    }

    public void setSemicircleRadius(float semicircleRadius) {
        if (this.semicircleRadius != semicircleRadius) {
            this.semicircleRadius = semicircleRadius;
            calculate();
            component.invalidate();
        }
    }

    public Color getSemicircleColor() {
        return semicircleColor;
    }

    public void setSemicircleColor(Color semicircleColor) {
        if (this.semicircleColor != semicircleColor) {
            this.semicircleColor = semicircleColor;
            calculate();
            component.invalidate();
        }
    }

    public float getDashLineLength() {
        return px2Vp(dashLineLength);
    }

    public void setDashLineLength(float dashLineLength) {
        if (this.dashLineLength != dashLineLength) {
            this.dashLineLength = dashLineLength;
            calculate();
            component.invalidate();
        }
    }

    public float getDashLineHeight() {
        return px2Vp(dashLineHeight);
    }

    public void setDashLineHeight(float dashLineHeight) {
        if (this.dashLineHeight != dashLineHeight) {
            this.dashLineHeight = dashLineHeight;
            calculate();
            component.invalidate();
        }
    }

    public float getDashLineGap() {
        return px2Vp(dashLineGap);
    }

    public void setDashLineGap(float dashLineGap) {
        if (this.dashLineGap != dashLineGap) {
            this.dashLineGap = dashLineGap;
            calculate();
            component.invalidate();
        }
    }

    public Color getDashLineColor() {
        return dashLineColor;
    }

    public void setDashLineColor(Color dashLineColor) {
        if (this.dashLineColor != dashLineColor) {
            this.dashLineColor = dashLineColor;
            calculate();
            component.invalidate();
        }
    }

    public boolean isSemicircleTop() {
        return isSemicircleTop;
    }

    public void setSemicircleTop(boolean semicircleTop) {
        if (this.isSemicircleTop != semicircleTop) {
            isSemicircleTop = semicircleTop;
            calculate();
            component.invalidate();
        }
    }

    public boolean isSemicircleBottom() {
        return isSemicircleBottom;
    }

    public void setSemicircleBottom(boolean semicircleBottom) {
        if (isSemicircleBottom != semicircleBottom) {
            isSemicircleBottom = semicircleBottom;
            calculate();
            component.invalidate();
        }
    }

    public boolean isSemicircleLeft() {
        return isSemicircleLeft;
    }

    public void setSemicircleLeft(boolean semicircleLeft) {
        if (isSemicircleLeft != semicircleLeft) {
            isSemicircleLeft = semicircleLeft;
            calculate();
            component.invalidate();
        }
    }

    public boolean isSemicircleRight() {
        return isSemicircleRight;
    }

    public void setSemicircleRight(boolean semicircleRight) {
        if (isSemicircleRight != semicircleRight) {
            isSemicircleRight = semicircleRight;
            calculate();
            component.invalidate();
        }
    }

    public boolean isDashLineTop() {
        return isDashLineTop;
    }

    public void setDashLineTop(boolean dashLineTop) {
        if (isDashLineTop != dashLineTop) {
            isDashLineTop = dashLineTop;
            calculate();
            component.invalidate();
        }
    }

    public boolean isDashLineBottom() {
        return isDashLineBottom;
    }

    public void setDashLineBottom(boolean dashLineBottom) {
        if (isDashLineBottom != dashLineBottom) {
            isDashLineBottom = dashLineBottom;
            calculate();
            component.invalidate();
        }
    }

    public boolean isDashLineLeft() {
        return isDashLineLeft;
    }

    public void setDashLineLeft(boolean dashLineLeft) {
        if (isDashLineLeft != dashLineLeft) {
            isDashLineLeft = dashLineLeft;
            calculate();
            component.invalidate();
        }
    }

    public boolean isDashLineRight() {
        return isDashLineRight;
    }

    public void setDashLineRight(boolean dashLineRight) {
        if (isDashLineRight != dashLineRight) {
            isDashLineRight = dashLineRight;
            calculate();
            component.invalidate();
        }
    }

    public float getDashLineMarginTop() {
        return px2Vp(dashLineMarginTop);
    }

    public void setDashLineMarginTop(float dashLineMarginTop) {
        if (this.dashLineMarginTop != dashLineMarginTop) {
            this.dashLineMarginTop = dashLineMarginTop;
            calculate();
            component.invalidate();
        }
    }

    public float getDashLineMarginBottom() {
        return px2Vp(dashLineMarginBottom);
    }

    public void setDashLineMarginBottom(float dashLineMarginBottom) {
        if (this.dashLineMarginBottom != dashLineMarginBottom) {
            this.dashLineMarginBottom = dashLineMarginBottom;
            calculate();
            component.invalidate();
        }
    }

    public float getDashLineMarginLeft() {
        return px2Vp(dashLineMarginLeft);
    }

    public void setDashLineMarginLeft(float dashLineMarginLeft) {
        if (this.dashLineMarginLeft != dashLineMarginLeft) {
            this.dashLineMarginLeft = dashLineMarginLeft;
            calculate();
            component.invalidate();
        }
    }

    public float getDashLineMarginRight() {
        return px2Vp(dashLineMarginRight);
    }

    public void setDashLineMarginRight(float dashLineMarginRight) {
        if (this.dashLineMarginRight != dashLineMarginRight) {
            this.dashLineMarginRight = dashLineMarginRight;
            calculate();
            component.invalidate();
        }
    }
}
