package yyydjk.com.couponview.automate;

import ohos.aafwk.ability.Ability;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.PageSlider;
import ohos.agp.components.TabList;
import org.junit.Before;
import org.junit.Test;
import yyydjk.com.couponview.MainAbility;
import yyydjk.com.couponview.ResourceTable;
import yyydjk.com.couponview.slice.MainAbilitySlice;

import java.lang.reflect.Field;
import java.util.List;

import static org.junit.Assert.*;

public class CouponViewAutomateTest {

    private Ability ability;

    @Before
    public void setUp() {
        ability = EventHelper.startAbility(MainAbility.class);
    }

    @Test
    public void testTab() throws Exception {
        TabList tabList = (TabList) ability.findComponentById(ResourceTable.Id_tab);
        assertNotNull("It's right", tabList);
        MainAbilitySlice slice = MainAbilitySlice.class.newInstance();
        //通过属性名， 获得属性引用
        Field field = MainAbilitySlice.class.getDeclaredField("titles");
        //调用私有的属性 必须临时修改访问修饰符检测
        field.setAccessible(true);
        //获得属性应用的值
        List<?> list = (List<?>) field.get(slice);
        assertNotNull("It's right", list);
        for (int i = 0; i < list.size(); i++) {
            EventHelper.triggerClickEvent(ability, tabList.getTabAt(i));
            assertEquals(tabList.getSelectedTab(), tabList.getTabAt(i));
        }
    }

    @Test
    public void testPageSlider() {
        PageSlider pageSlider = (PageSlider) ability.findComponentById(ResourceTable.Id_pageSlider);
        TabList tabList = (TabList) ability.findComponentById(ResourceTable.Id_tab);
        assertNotNull("It's right", pageSlider);
        EventHelper.inputSwipe(ability, pageSlider, 50, 1000, 1000, 1000, 1000);
        assertEquals(pageSlider.getCurrentPage(), tabList.getSelectedTab().getPosition());
    }
}
