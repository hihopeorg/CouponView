package yyydjk.com.couponview.unit;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.components.Attr;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.utils.Color;
import ohos.app.Context;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import yyydjk.com.library.CouponViewHelper;

import java.util.Optional;

import static junit.framework.TestCase.*;

public class CouponViewHelperTest {
    private static IAbilityDelegator abilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
    private CouponViewHelper helper;
    private Context mContext;
    private AttrSet attrSet;

    @Before
    public void setUp() {
        mContext = abilityDelegator.getAppContext();
        attrSet = new AttrSet() {
            @Override
            public Optional<String> getStyle() {
                return Optional.empty();
            }

            @Override
            public int getLength() {
                return 0;
            }

            @Override
            public Optional<Attr> getAttr(int i) {
                return Optional.empty();
            }

            @Override
            public Optional<Attr> getAttr(String s) {
                return Optional.empty();
            }
        };
        helper = new CouponViewHelper(new Component(mContext), mContext, attrSet, "");
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void setSemicircleGap() {
        helper.setSemicircleGap(30);
        float semicircle = helper.getSemicircleGap();
        assertEquals(30 / AttrHelper.getDensity(mContext), semicircle);
    }

    @Test
    public void setSemicircleRadius() {
        helper.setSemicircleRadius(30);
        float semicircle = helper.getSemicircleRadius();
        assertEquals(30 / AttrHelper.getDensity(mContext), semicircle);
    }

    @Test
    public void setSemicircleColor() {
        helper.setSemicircleColor(Color.BLUE);
        Color semicircle = helper.getSemicircleColor();
        assertEquals(Color.BLUE, semicircle);
    }

    @Test
    public void setDashLineLength() {
        helper.setDashLineLength(30);
        float length = helper.getDashLineLength();
        assertEquals(30 / AttrHelper.getDensity(mContext), length);
    }

    @Test
    public void setDashLineHeight() {
        helper.setDashLineHeight(30);
        float height = helper.getDashLineHeight();
        assertEquals(30 / AttrHelper.getDensity(mContext), height);
    }

    @Test
    public void setDashLineGap() {
        helper.setDashLineGap(30);
        float lineGap = helper.getDashLineGap();
        assertEquals(30 / AttrHelper.getDensity(mContext), lineGap);
    }

    @Test
    public void setDashLineColor() {
        helper.setDashLineColor(Color.BLUE);
        Color lineColor = helper.getDashLineColor();
        assertEquals(Color.BLUE, lineColor);
    }

    @Test
    public void setSemicircleTop() {
        helper.setSemicircleTop(false);
        boolean semicircleTop = helper.isSemicircleTop();
        assertFalse("It is right", semicircleTop);
    }

    @Test
    public void setSemicircleBottom() {
        helper.setSemicircleBottom(false);
        boolean semicircleBottom = helper.isSemicircleBottom();
        assertFalse("It is right", semicircleBottom);
    }

    @Test
    public void setSemicircleLeft() {
        helper.setSemicircleLeft(true);
        boolean semicircleLeft = helper.isSemicircleLeft();
        assertTrue("It is right", semicircleLeft);
    }

    @Test
    public void setSemicircleRight() {
        helper.setSemicircleRight(true);
        boolean semicircleRight = helper.isSemicircleRight();
        assertTrue("It is right", semicircleRight);
    }

    @Test
    public void setDashLineTop() {
        helper.setDashLineTop(true);
        boolean lineTop = helper.isDashLineTop();
        assertTrue("It is right", lineTop);
    }

    @Test
    public void setDashLineBottom() {
        helper.setDashLineBottom(true);
        boolean lineBottom = helper.isDashLineBottom();
        assertTrue("It is right", lineBottom);
    }

    @Test
    public void setDashLineLeft() {
        helper.setDashLineLeft(false);
        boolean lineLeft = helper.isDashLineLeft();
        assertFalse("It is right", lineLeft);
    }

    @Test
    public void setDashLineRight() {
        helper.setDashLineRight(false);
        boolean lineRight = helper.isDashLineRight();
        assertFalse("It is right", lineRight);
    }

    @Test
    public void setDashLineMarginTop() {
        helper.setDashLineMarginTop(30);
        float marginTop = helper.getDashLineMarginTop();
        assertEquals(30 / AttrHelper.getDensity(mContext), marginTop);
    }

    @Test
    public void setDashLineMarginBottom() {
        helper.setDashLineMarginBottom(30);
        float marginBottom = helper.getDashLineMarginBottom();
        assertEquals(30 / AttrHelper.getDensity(mContext), marginBottom);
    }

    @Test
    public void setDashLineMarginLeft() {
        helper.setDashLineMarginLeft(30);
        float marginLeft = helper.getDashLineMarginLeft();
        assertEquals(30 / AttrHelper.getDensity(mContext), marginLeft);
    }

    @Test
    public void setDashLineMarginRight() {
        helper.setDashLineMarginRight(30);
        float marginRight = helper.getDashLineMarginRight();
        assertEquals(30 / AttrHelper.getDensity(mContext), marginRight);
    }
}