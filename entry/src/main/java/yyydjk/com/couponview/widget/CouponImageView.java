package yyydjk.com.couponview.widget;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.render.Canvas;
import ohos.agp.utils.Color;
import ohos.app.Context;
import yyydjk.com.library.CouponViewHelper;

public class CouponImageView extends Image implements Component.DrawTask, Component.EstimateSizeListener {
    private CouponViewHelper helper;

    public CouponImageView(Context context) {
        this(context, null);
    }

    public CouponImageView(Context context, AttrSet attrSet) {
        this(context, attrSet, "");
    }

    public CouponImageView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        helper = new CouponViewHelper(this, context, attrSet, styleName);
        addDrawTask(this);
        setEstimateSizeListener(this);
    }

    @Override
    public boolean onEstimateSize(int widthEstimateConfig, int heightEstimateConfig) {
        int width = EstimateSpec.getSize(widthEstimateConfig);
        int height = EstimateSpec.getSize(heightEstimateConfig);
        helper.onSizeChanged(width, height);
        return false;
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        helper.onDraw(canvas);
    }

    public float getSemicircleGap() {
        return helper.getSemicircleGap();
    }

    public void setSemicircleGap(float semicircleGap) {
        helper.setSemicircleGap(semicircleGap);
    }

    public float getSemicircleRadius() {
        return helper.getSemicircleRadius();
    }

    public void setSemicircleRadius(float semicircleRadius) {
        helper.setSemicircleRadius(semicircleRadius);
    }

    public Color getSemicircleColor() {
        return helper.getSemicircleColor();
    }

    public void setSemicircleColor(Color semicircleColor) {
        helper.setSemicircleColor(semicircleColor);
    }

    public float getDashLineLength() {
        return helper.getDashLineLength();
    }

    public void setDashLineLength(float dashLineLength) {
        helper.setDashLineLength(dashLineLength);
    }

    public float getDashLineHeight() {
        return helper.getDashLineHeight();
    }

    public void setDashLineHeight(float dashLineHeight) {
        helper.setDashLineHeight(dashLineHeight);
    }

    public float getDashLineGap() {
        return helper.getDashLineGap();
    }

    public void setDashLineGap(float dashLineGap) {
        helper.setDashLineGap(dashLineGap);
    }

    public Color getDashLineColor() {
        return helper.getDashLineColor();
    }

    public void setDashLineColor(Color dashLineColor) {
        helper.setDashLineColor(dashLineColor);
    }

    public boolean isSemicircleTop() {
        return helper.isSemicircleTop();
    }

    public void setSemicircleTop(boolean semicircleTop) {
        helper.setSemicircleTop(semicircleTop);
    }

    public boolean isSemicircleBottom() {
        return helper.isSemicircleBottom();
    }

    public void setSemicircleBottom(boolean semicircleBottom) {
        helper.setSemicircleBottom(semicircleBottom);
    }

    public boolean isSemicircleLeft() {
        return helper.isSemicircleLeft();
    }

    public void setSemicircleLeft(boolean semicircleLeft) {
        helper.setSemicircleLeft(semicircleLeft);
    }

    public boolean isSemicircleRight() {
        return helper.isSemicircleRight();
    }

    public void setSemicircleRight(boolean semicircleRight) {
        helper.setSemicircleRight(semicircleRight);
    }

    public boolean isDashLineTop() {
        return helper.isDashLineTop();
    }

    public void setDashLineTop(boolean dashLineTop) {
        helper.setDashLineTop(dashLineTop);
    }

    public boolean DashLineBottom() {
        return helper.isDashLineBottom();
    }

    public void setDashLineBottom(boolean dashLineBottom) {
        helper.setDashLineBottom(dashLineBottom);
    }

    public boolean isDashLineLeft() {
        return helper.isDashLineLeft();
    }

    public void setDashLineLeft(boolean dashLineLeft) {
        helper.setDashLineLeft(dashLineLeft);
    }

    public boolean isDashLineRight() {
        return helper.isDashLineRight();
    }

    public void setDashLineRight(boolean dashLineRight) {
        helper.setDashLineRight(dashLineRight);
    }

    public float getDashLineMarginTop() {
        return helper.getDashLineMarginTop();
    }

    public void setDashLineMarginTop(float dashLineMarginTop) {
        helper.setDashLineMarginTop(dashLineMarginTop);
    }

    public float getDashLineMarginBottom() {
        return helper.getDashLineMarginBottom();
    }

    public void setDashLineMarginBottom(float dashLineMarginBottom) {
        helper.setDashLineMarginBottom(dashLineMarginBottom);
    }

    public float getDashLineMarginLeft() {
        return helper.getDashLineMarginLeft();
    }

    public void setDashLineMarginLeft(float dashLineMarginLeft) {
        helper.setDashLineMarginLeft(dashLineMarginLeft);
    }

    public float getDashLineMarginRight() {
        return helper.getDashLineMarginRight();
    }

    public void setDashLineMarginRight(float dashLineMarginRight) {
        helper.setDashLineMarginRight(dashLineMarginRight);
    }
}