package yyydjk.com.couponview.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;
import yyydjk.com.couponview.ResourceTable;
import yyydjk.com.library.CouponView;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class MainAbilitySlice extends AbilitySlice implements Component.TouchEventListener {

    private TabList mTab;
    private PageSlider mPager;
    private List<String> titles = Arrays.asList("半圆边缘", "虚线边框", "半圆虚线", "自定义属性", "自定义ImageView");
    private float currentX;
    private LayoutScatter scatter;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        scatter = LayoutScatter.getInstance(this);
        mTab = (TabList) findComponentById(ResourceTable.Id_tab);
        mTab.setTouchEventListener(this);
        mPager = (PageSlider) findComponentById(ResourceTable.Id_pageSlider);
        mPager.setProvider(new MyPagerAdapter());
        mPager.setCurrentPage(0);
        mPager.setOrientation(Component.HORIZONTAL);
        mPager.setSlidingPossible(true);
        mTab.addTabSelectedListener(new TabList.TabSelectedListener() {
            @Override
            public void onSelected(TabList.Tab tab) {
                mPager.setCurrentPage(tab.getPosition());
                Optional<Display> display = DisplayManager.getInstance().getDefaultDisplay(getContext());
                DisplayAttributes displayAttributes = display.get().getAttributes();
                int x = displayAttributes.width >> 1;
                int componentX = (tab.getRight() - tab.getLeft()) / 2 + tab.getLeft();
                if (x > currentX) {
                    if (componentX > x) {
                        mTab.fluentScrollByX(-(componentX - x) >> 1);
                    } else {
                        mTab.fluentScrollByX((componentX - x) >> 1);
                    }
                } else {
                    mTab.fluentScrollByX((componentX - x) >> 1);
                }
            }

            @Override
            public void onUnselected(TabList.Tab tab) {

            }

            @Override
            public void onReselected(TabList.Tab tab) {

            }
        });
        mPager.addPageChangedListener(new PageSlider.PageChangedListener() {
            @Override
            public void onPageSliding(int itemPos, float itemPosOffset, int itemPosOffsetPixels) {

            }

            @Override
            public void onPageSlideStateChanged(int state) {

            }

            @Override
            public void onPageChosen(int itemPos) {
                mTab.selectTab(mTab.getTabAt(itemPos));
            }
        });
        initTab();
    }

    private void initTab() {
        for (int i = 0; i < titles.size(); i++) {
            TabList.Tab tab = mTab.new Tab(this);
            tab.setText(titles.get(i));
            mTab.addTab(tab);
        }
        mTab.getTabAt(0).select();
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    public class MyPagerAdapter extends PageSliderProvider {

        @Override
        public int getCount() {
            return titles.size();
        }

        @Override
        public Object createPageInContainer(ComponentContainer componentContainer, int position) {
            Component component = null;
            TableLayoutManager manager;
            if (componentContainer != null) {
                switch (position) {
                    case 0:
                        component = scatter.parse(ResourceTable.Layout_page_coupon_list, null, false);
                        ListContainer listContainer0 = (ListContainer) component.findComponentById(ResourceTable.Id_listContainer);
                        listContainer0.setItemProvider(new BaseItemProvider() {
                            String[] intArray = getContext().getStringArray(ResourceTable.Strarray_colors);

                            @Override
                            public int getCount() {
                                return intArray.length;
                            }

                            @Override
                            public Object getItem(int position) {
                                return null;
                            }

                            @Override
                            public long getItemId(int position) {
                                return 0;
                            }

                            @Override
                            public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
                                Component view = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_item_coupon_semi_circle, componentContainer, false);
                                CouponView couponView = (CouponView) view.findComponentById(ResourceTable.Id_couponView);
                                ShapeElement element = new ShapeElement();
                                element.setRgbColor(RgbColor.fromArgbInt(Color.getIntColor(intArray[position])));
                                couponView.setBackground(element);
                                return view;
                            }
                        });
                        break;
                    case 1:
                        component = scatter.parse(ResourceTable.Layout_page_coupon_list, null, false);
                        ListContainer listContainer1 = (ListContainer) component.findComponentById(ResourceTable.Id_listContainer);
                        TableLayoutManager tableLayoutManager1 = new TableLayoutManager();
                        tableLayoutManager1.setColumnCount(2);
                        listContainer1.setLayoutManager(tableLayoutManager1);
                        listContainer1.setItemProvider(new BaseItemProvider() {
                            String[] intArray = getContext().getStringArray(ResourceTable.Strarray_colors);

                            @Override
                            public int getCount() {
                                return intArray.length;
                            }

                            @Override
                            public Object getItem(int position) {
                                return null;
                            }

                            @Override
                            public long getItemId(int position) {
                                return 0;
                            }

                            @Override
                            public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
                                Component view = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_item_coupon_dash_line, componentContainer, false);
                                CouponView couponView = (CouponView) view.findComponentById(ResourceTable.Id_couponView);
                                ShapeElement element = new ShapeElement();
                                element.setRgbColor(RgbColor.fromArgbInt(Color.getIntColor(intArray[position])));
                                couponView.setBackground(element);
                                return view;
                            }
                        });
                        break;
                    case 2:
                        component = scatter.parse(ResourceTable.Layout_page_coupon_list, null, false);
                        ListContainer listContainer2 = (ListContainer) component.findComponentById(ResourceTable.Id_listContainer);
                        TableLayoutManager tableLayoutManager2 = new TableLayoutManager();
                        tableLayoutManager2.setColumnCount(2);
                        listContainer2.setLayoutManager(tableLayoutManager2);
                        listContainer2.setItemProvider(new BaseItemProvider() {
                            String[] intArray = getContext().getStringArray(ResourceTable.Strarray_colors);

                            @Override
                            public int getCount() {
                                return intArray.length;
                            }

                            @Override
                            public Object getItem(int position) {
                                return null;
                            }

                            @Override
                            public long getItemId(int position) {
                                return 0;
                            }

                            @Override
                            public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
                                Component view = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_item_coupon_combination, componentContainer, false);
                                CouponView couponView = (CouponView) view.findComponentById(ResourceTable.Id_couponView);
                                ShapeElement element = new ShapeElement();
                                element.setRgbColor(RgbColor.fromArgbInt(Color.getIntColor(intArray[position])));
                                couponView.setBackground(element);
                                return view;
                            }
                        });
                        break;
                    case 3:
                        component = scatter.parse(ResourceTable.Layout_page_coupon_custom, null, false);
                        setListener(component);
                        break;
                    case 4:
                        component = scatter.parse(ResourceTable.Layout_page_coupon_image, null, false);
                        break;
                }
                componentContainer.addComponent(component);
            }
            return component;
        }

        @Override
        public void destroyPageFromContainer(ComponentContainer componentContainer, int position, Object obj) {
            if (componentContainer == null) {
                return;
            }
            if (obj instanceof Component) {
                componentContainer.removeComponent((Component) obj);
            }
        }

        @Override
        public boolean isPageMatchToObject(Component component, Object o) {
            return component == o;
        }

        @Override
        public String getPageTitle(int position) {
            return titles.get(position);
        }
    }

    private void setListener(Component component) {
        CouponView mCouponView = (CouponView) component.findComponentById(ResourceTable.Id_couponView);
        Checkbox mSemicircleTop = (Checkbox) component.findComponentById(ResourceTable.Id_semicircle_top);
        Checkbox mSemicircleBottom = (Checkbox) component.findComponentById(ResourceTable.Id_semicircle_bottom);
        Checkbox mSemicircleLeft = (Checkbox) component.findComponentById(ResourceTable.Id_semicircle_left);
        Checkbox mSemicircleRight = (Checkbox) component.findComponentById(ResourceTable.Id_semicircle_right);
        Checkbox mDashLineTop = (Checkbox) component.findComponentById(ResourceTable.Id_dash_line_top);
        Checkbox mDashLineBottom = (Checkbox) component.findComponentById(ResourceTable.Id_dash_line_bottom);
        Checkbox mDashLineLeft = (Checkbox) component.findComponentById(ResourceTable.Id_dash_line_left);
        Checkbox mDashLineRight = (Checkbox) component.findComponentById(ResourceTable.Id_dash_line_right);
        Slider mSbSemicircleRadius = (Slider) component.findComponentById(ResourceTable.Id_sbSemicircleRadius);
        Slider mSbSemicircleCap = (Slider) component.findComponentById(ResourceTable.Id_sbSemicircleCap);
        Slider mSbDashLineLength = (Slider) component.findComponentById(ResourceTable.Id_sbDashLineLength);
        Slider mSbDashLineGap = (Slider) component.findComponentById(ResourceTable.Id_sbDashLineGap);
        Slider mSbDashLineHeight = (Slider) component.findComponentById(ResourceTable.Id_sbDashLineHeight);
        Slider mSbTopDashLineMargin = (Slider) component.findComponentById(ResourceTable.Id_sbTopDashLineMargin);
        Slider mSbBottomDashLineMargin = (Slider) component.findComponentById(ResourceTable.Id_sbBottomDashLineMargin);
        Slider mSbLeftDashLineMargin = (Slider) component.findComponentById(ResourceTable.Id_sbLeftDashLineMargin);
        Slider mSbRightDashLineMargin = (Slider) component.findComponentById(ResourceTable.Id_sbRightDashLineMargin);

        mSemicircleTop.setCheckedStateChangedListener((buttonView, isChecked) -> mCouponView.setSemicircleTop(isChecked));
        mSemicircleBottom.setCheckedStateChangedListener((buttonView, isChecked) -> mCouponView.setSemicircleBottom(isChecked));
        mSemicircleLeft.setCheckedStateChangedListener((buttonView, isChecked) -> mCouponView.setSemicircleLeft(isChecked));
        mSemicircleRight.setCheckedStateChangedListener((buttonView, isChecked) -> mCouponView.setSemicircleRight(isChecked));

        mDashLineTop.setCheckedStateChangedListener((buttonView, isChecked) -> mCouponView.setDashLineTop(isChecked));
        mDashLineBottom.setCheckedStateChangedListener((buttonView, isChecked) -> mCouponView.setDashLineBottom(isChecked));
        mDashLineLeft.setCheckedStateChangedListener((buttonView, isChecked) -> mCouponView.setDashLineLeft(isChecked));
        mDashLineRight.setCheckedStateChangedListener((buttonView, isChecked) -> mCouponView.setDashLineRight(isChecked));

        mSbSemicircleRadius.setProgressValue((int) mCouponView.getSemicircleRadius());
        mSbSemicircleRadius.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int progress, boolean fromUser) {
                mCouponView.setSemicircleRadius(vp2Px(progress));
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {

            }
        });
        mSbSemicircleCap.setProgressValue((int) mCouponView.getSemicircleGap());
        mSbSemicircleCap.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int progress, boolean fromUser) {
                mCouponView.setSemicircleGap(vp2Px(progress));
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {

            }
        });

        mSbDashLineLength.setProgressValue((int) mCouponView.getDashLineLength());
        mSbDashLineLength.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int progress, boolean fromUser) {
                mCouponView.setDashLineLength(vp2Px(progress));
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {

            }
        });

        mSbDashLineGap.setProgressValue((int) mCouponView.getDashLineGap());
        mSbDashLineGap.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int progress, boolean fromUser) {
                mCouponView.setDashLineGap(vp2Px(progress));
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {

            }
        });

        mSbDashLineHeight.setProgressValue((int) mCouponView.getDashLineHeight() * 10);
        mSbDashLineHeight.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int progress, boolean fromUser) {
                mCouponView.setDashLineHeight(vp2Px(progress) / 10);
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {

            }
        });

        mSbTopDashLineMargin.setProgressValue((int) mCouponView.getDashLineMarginTop());
        mSbTopDashLineMargin.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int progress, boolean fromUser) {
                mCouponView.setDashLineMarginTop(vp2Px(progress));
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {

            }
        });


        mSbBottomDashLineMargin.setProgressValue((int) mCouponView.getDashLineMarginBottom());
        mSbBottomDashLineMargin.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int progress, boolean fromUser) {
                mCouponView.setDashLineMarginBottom(vp2Px(progress));
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {

            }
        });

        mSbLeftDashLineMargin.setProgressValue((int) mCouponView.getDashLineMarginLeft());
        mSbLeftDashLineMargin.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int progress, boolean fromUser) {
                mCouponView.setDashLineMarginLeft(vp2Px(progress));
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {

            }
        });

        mSbRightDashLineMargin.setProgressValue((int) mCouponView.getDashLineMarginRight());
        mSbRightDashLineMargin.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int progress, boolean fromUser) {
                mCouponView.setDashLineMarginRight(vp2Px(progress));
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {

            }
        });
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        MmiPoint point = touchEvent.getPointerScreenPosition(touchEvent.getIndex());
        currentX = point.getX();
        return false;
    }
    private int vp2Px(float vp) {
        return (int) (vp * AttrHelper.getDensity(this));
    }
}
