# CouponView

**本项目是基于开源项目CouponView进行ohos化的移植和开发的，可以通过项目标签以及github地址（https://github.com/dongjunkun/CouponView ）追踪到原项目版本**

#### 项目介绍

- 项目名称：优惠券效果

- 所属系列：ohos的第三方组件适配移植

- 功能：

  1.优惠券的半圆锯齿效果
  
  2.优惠券的虚线边框效果

- 项目移植状态：完成

- 调用差异：无

- 项目作者和维护人：hihope

- 联系方式：hihope@hoperun.com

- 原项目Doc地址：https://github.com/dongjunkun/CouponView

- 原项目基线版本：v1.0.4,sha1:6c56a997d7c9c62e2367260cf9d03ec81e454653

- 编程语言：Java

#### 效果展示
<img src="screenshot/效果展示.gif"/>

#### 安装教程

方案一：

1. 添加har包到lib文件夹内。

2. 在entry的build.gradle内添加如下代码。

```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.har'])
	……
}
```

方案二：

1.在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址:

```
repositories {
    maven {
        url 'http://106.15.92.248:8081/repository/Releases/' 
    }
}
```

2.在应用模块的build.gradle的dependencies闭包中，添加如下代码:

```
dependencies {
    implementation 'yyydjk.com.library:CouponView:1.0.0'
}
```

#### 使用说明

1. 自定义属性说明

| 自定义属性 |格式|说明|
| :---:|:---:|:---: |
|cv_dash_line_color|color|虚线的颜色|
|cv_dash_line_gap|dimension|虚线的间隔|
|cv_dash_line_height|dimension|虚线的高度|
|cv_dash_line_length|dimension|虚线的长度|
|cv_semicircle_color|color|半圆的颜色，一般需要和背景色一致|
|cv_semicircle_gap|dimension|半圆之前的间隔|
|cv_semicircle_radius|dimension|半圆的半径|
|cv_is_semicircle_top|boolean|是否绘制顶部半圆锯齿|
|cv_is_semicircle_bottom|boolean|是否绘制底部半圆锯齿|
|cv_is_semicircle_left|boolean|是否绘制左侧半圆锯齿|
|cv_is_semicircle_right|boolean|是否绘制右侧半圆锯齿|
|cv_is_dash_line_top|boolean|是否绘制顶部虚线|
|cv_is_dash_line_bottom|boolean|是否绘制底部虚线|
|cv_is_dash_line_left|boolean|是否绘制左侧虚线|
|cv_is_dash_line_right|boolean|是否绘制右侧虚线|
|cv_dash_line_margin_top|dimension|顶部虚线距离View顶部的距离|
|cv_dash_line_margin_bottom|dimension|底部虚线距离View底部的距离|
|cv_dash_line_margin_left|dimension|左侧虚线距离View左侧的距离|
|cv_dash_line_margin_right|dimension|右侧虚线距离View右侧的距离|

2. xml中使用
   
   ```
    <yyydjk.com.library.CouponView
            xmlns:ohos="http://schemas.huawei.com/res/ohos"
            xmlns:app="http://schemas.huawei.com/res/ohos-auto"
            ohos:id="$+id:couponView"
            ohos:height="150vp"
            ohos:width="match_parent"
            ohos:background_element="#4CAF50"
            ohos:margin="15vp"
            app:cv_dash_line_color="white"
            app:cv_dash_line_gap="10vp"
            app:cv_dash_line_height="2vp"
            app:cv_dash_line_length="10vp"
            app:cv_dash_line_margin_bottom="5vp"
            app:cv_dash_line_margin_left="50vp"
            app:cv_dash_line_margin_right="50vp"
            app:cv_dash_line_margin_top="5vp"
            app:cv_semicircle_color="white"
            app:cv_semicircle_gap="8vp"
            app:cv_semicircle_radius="4vp"/>
 
   ```  
   
3. 代码中动态设置

![couponTip.png](/screenshot/couponTip.png)  

4. 定制自己的component
   
   ```
    public class CouponImageView extends Image implements Component.DrawTask, Component.EstimateSizeListener {
        private CouponViewHelper helper;
    
        public CouponImageView(Context context) {
            this(context, null);
        }
    
        public CouponImageView(Context context, AttrSet attrSet) {
            this(context, attrSet, "");
        }
    
        public CouponImageView(Context context, AttrSet attrSet, String styleName) {
            super(context, attrSet, styleName);
            helper = new CouponViewHelper(this, context, attrSet, styleName);
            addDrawTask(this);
            setEstimateSizeListener(this);
        }
    
        @Override
        public boolean onEstimateSize(int widthEstimateConfig, int heightEstimateConfig) {
            int width = EstimateSpec.getSize(widthEstimateConfig);
            int height = EstimateSpec.getSize(heightEstimateConfig);
            helper.onSizeChanged(width, height);
            return false;
        }
    
        @Override
        public void onDraw(Component component, Canvas canvas) {
            helper.onDraw(canvas);
        }
    
        public float getSemicircleGap() {
            return helper.getSemicircleGap();
        }
    
        public void setSemicircleGap(float semicircleGap) {
            helper.setSemicircleGap(semicircleGap);
        }
 
       ......
    }
   ```  

#### 版本迭代

- v1.0.0

实现功能

- 优惠券半圆锯齿和虚线边框效果


#### 版权和许可信息
```
Copyright 2016 dongjunkun

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License. 
```